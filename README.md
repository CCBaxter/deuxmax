# Install to production

Generate build 

```shell
nvm install
npm install
npm run build
```

Add credentials to production site 

```dotenv
DEPLOY_URL=https://...
DEPLOY_API_KEY=
```

**(For generate API Key, you should create custom integrations in  Settings > Integrations > Add custom integrations)**

```shell
npm run deloy
```

(Add env var `NODE_TLS_REJECT_UNAUTHORIZED=0` if you upload to self-signed certificate host)

## Add cookies

Go to admin > Settings > Code Injection

And add code below in "Site Footer" between `<script></script>`

```javascript
  COOKIES.push({
    name: "{CHOOSE_NAME}",
    checked: false,
    disabled: false,
    fn: function() {
        // Code provide from Google Analytics or Matomo
      }
  });
```

# Development

Initialize environment variables

```shell
cp .env.dist .env
```

Start docker

```
docker-compose up -d
```

Download dependencies

```bash
nvm install
npm install 
```

Launch gulp & browser sync

```shell
npm start
```