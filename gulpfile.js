require('dotenv').config();
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const cleanCSS = require('gulp-clean-css');
const merge = require('merge-stream');
const zip = require('gulp-zip');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const GhostAdminApi = require('@tryghost/admin-api');
const purgecss = require('gulp-purgecss');

const safeClasses = [
'kg-width-wide', 'kg-width-full', 'kg-gallery-card', 'kg-gallery-container', 'kg-gallery-row', 'kg-gallery-image', 'kg-bookmark-card', 'kg-bookmark-container',
'kg-bookmark-content',
'kg-bookmark-title',
'kg-bookmark-description',
'kg-bookmark-metadata',
'kg-bookmark-icon',
'kg-bookmark-author',
'kg-bookmark-publisher',
'kg-bookmark-thumbnail'];

// Build
gulp.task('webfonts-cp', function() {
  return gulp.src([
    './node_modules/@fortawesome/fontawesome-free/webfonts/*'
  ])
    .pipe(gulp.dest('./theme/assets/webfonts/'));
});
gulp.task('lib-cp', function() {
  var sassStream,
    cssStream;


  //select additional css files
  cssStream = gulp.src('./theme/lib/isso.css')
    .pipe(concat('isso.css'))

  //compile sass
  sassStream = gulp.src('./theme/sass/comment.scss', {
    allowEmpty: false,
  })
    .pipe(sass());


  //merge the two streams and concatenate their contents into a single file
  return merge(sassStream, cssStream)
    .pipe(concat('isso.css'))
    .pipe(gulp.dest('./theme/assets/'));
});
gulp.task('sass', function () {
  return gulp.src('./theme/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(concat('style.css'))
    .pipe(purgecss({
      content: ['./theme/**/*.hbs'],
      safelist: safeClasses,
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./theme/assets/'));
});
gulp.task('js', function () {
  return gulp.src([
    './node_modules/jquery/dist/jquery.min.js',
    './theme/js/**/*.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./theme/assets'));
});


// Production
gulp.task('pack', function () {
  return gulp.src([
    './theme/**',
    '!./theme/config',
    '!./theme/js',
    '!./theme/sass',
  ])
    .pipe(zip('writer.zip'))
    .pipe(gulp.dest('./build'));
});
gulp.task('send', async (done) => {
  try {
    const url = process.env.DEPLOY_URL;
    const admin_api_key = process.env.DEPLOY_API_KEY;
    const zipFile = 'build/writer.zip';
    const api = new GhostAdminApi({
      url,
      key: admin_api_key,
      version: 'v5.0'
    });
    await api.themes.upload({file: zipFile});
    console.log(`${zipFile} successfully uploaded.`);
    done();
  } catch(err) {
    console.error(err);
    done(err);
  }
});


// Development
gulp.task('js-sourcemap', function() {
  return gulp.src('./theme/assets/script.js')
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./theme/assets'));
});
gulp.task('watch', function () {
  gulp.watch('./theme/js/**/*.js', gulp.series('js', 'reload'));
  gulp.watch('./theme/sass/**/*.scss', gulp.series(gulp.parallel('lib-cp', 'sass'), 'sass-watch'));
  gulp.watch('./theme/**/*.hbs', gulp.series('reload'))
});
gulp.task('sass-watch', function () {
  return gulp.src('./theme/assets/*.css')
    .pipe(gulp.dest('./theme/assets/'))
    .pipe(browserSync.stream());
});
gulp.task('reload', function(done) {
  browserSync.reload();
  done();
});
gulp.task('serve', function () {
  browserSync.init({
    baseDir: './theme',
    https: true,
    proxy: process.env.GHOST_URL,
    open: false,
    notify: false,
    plugins: []
  });
});

// Tasks
gulp.task('default',  gulp.series('webfonts-cp', 'lib-cp', 'sass', 'js', 'js-sourcemap', gulp.parallel('watch', 'serve')));
gulp.task('build',  gulp.series('webfonts-cp', 'lib-cp', 'sass', 'js', 'pack'));
gulp.task('deploy',  gulp.series('send'));
