$().ready(function () {
  function hideLoginForm() {
    $('#member-connect-form').removeClass('on').addClass('off');
  }

  function hideSubscribeForm() {
    $('#member-subscribe-form').removeClass('on').addClass('off');
  }

  $('.btn-cta-login').on('click', function() {
    hideSubscribeForm();
    $('#member-connect-form').removeClass('off').addClass('on');
  });

  $('#member-connect-form .cta-close').on('click', function() {
    hideLoginForm();
  });

  $('.btn-cta-subscribe').on('click', function() {
    hideLoginForm();
    $('#member-subscribe-form').removeClass('off').addClass('on');
  });

  $('#member-subscribe-form .cta-close').on('click', function() {
    hideSubscribeForm();
  });
});
