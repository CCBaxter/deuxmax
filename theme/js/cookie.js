$().ready(function () {
  var cookieConsent = $('#cookie-consent');
  var FUNCTIONAL_COOKIE = {
    checked: true,
    disabled: true,
    name: 'Fonctionnels',
    fn: function() {},
  };

  COOKIES.unshift(FUNCTIONAL_COOKIE);
  var list = cookieConsent.find('.cookies-list');

  function slugify(name) {
    return name
      .replace(/\s+/g, '-')
      .replace(/\((.*)\)/g, '$1')
      .toLowerCase();
  }

  COOKIES.map(function (cookie) {
    var input =
      $('<input/>')
        .attr('id', `check-${slugify(cookie.name)}`)
        .attr('type', 'checkbox')
        .addClass('form-check-input');

    if (cookie.checked) {
      input.attr('checked', 'checked');
    }

    if (cookie.disabled) {
      input.attr('disabled', 'disabled');
    }

    list.append(
      $('<div/>')
        .addClass("form-check")
        .append(input)
        .append(
          $('<label/>')
            .attr('for', `check-${slugify(cookie.name)}`)
            .addClass('ml-2 form-check-label')
            .text(cookie.name)
        )
    );
  })

  function addCookie(name) {
    var cookies_accepted = localStorage.getItem('cookie-consent');
    if (cookies_accepted) {
      cookies_accepted = JSON.parse(cookies_accepted);
    } else {
      cookies_accepted = [];
    }

    if (cookies_accepted.indexOf(name) === -1) {
      cookies_accepted.push(name);
    }

    localStorage.setItem('cookie-consent', JSON.stringify(cookies_accepted));
  }

  function rmCookie(name) {
    var cookies_accepted = localStorage.getItem('cookie-consent');
    if (cookies_accepted) {
      cookies_accepted = JSON.parse(cookies_accepted);
    } else {
      cookies_accepted = [];
    }

    if (name !== 'Fonctionnels' && cookies_accepted.indexOf(name) !== -1) {
      cookies_accepted.splice(cookies_accepted.indexOf(name), 1);
    }

    localStorage.setItem('cookie-consent', JSON.stringify(cookies_accepted));
  }

  function init() {
    var consent = localStorage.getItem('cookie-consent');
    if (consent) {
      consent = JSON.parse(consent);
    } else {
      consent = [];
    }

    if(consent.length === 0) {
      cookieConsent.css("cssText", "display: flex !important;");
    } else {
      cookieConsent.css("cssText", "display: none !important;");
      consent.map(function(cookie) {
        var pos;
        if ((pos = COOKIES.map(function (cookie) { return cookie.name; }).indexOf(cookie)) !== -1) {
          if(COOKIES[pos].fn) {
            COOKIES[pos].fn();
          } else {
            console.error('Your function cookies is inexistant');
          }
        }
      });
    }
  }

  cookieConsent.find('.cta-accept').on('click', function() {
    COOKIES.map(function (cookie) {
      addCookie(cookie.name);
    });
    init();
  });

  cookieConsent.find('.cta-reject').on('click', function() {
    addCookie(FUNCTIONAL_COOKIE.name);
    COOKIES.map(function (cookie) {
      rmCookie(cookie.name);
    });
    init();
  });

  cookieConsent.find('.cta-submit').on('click', function() {
    addCookie(FUNCTIONAL_COOKIE.name);
    COOKIES.map(function (cookie) {
      if(cookieConsent.find(`#check-${slugify(cookie.name)}`).is(":checked")) {
        addCookie(cookie.name);
      }
    });
    init();
  });

  init();
});
